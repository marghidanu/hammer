# Hammer

[![pipeline status](https://gitlab.com/marghidanu/hammer/badges/master/pipeline.svg)](https://gitlab.com/marghidanu/hammer/commits/master)

## Description

## Development environment

	vagrant up
	vagrant ssh

	perl Build.PL
	./Build installdeps
	./Build
	./Build test

## Installation

### From repository

```bash
cpanm git://gitlab.com/marghidanu/hammer.git
```

### Binaries

[TODO]

## Documentation

### Creating a configuration

[TODO]

**hammer.yaml**

```yaml
name: Automation example
description: Just a small example on how to use Hammer

variables:
    NAME: John Doe
    ITEMS: 5

default:
    dependencies:
        - first
        - second
        - third
    script:
        - pwd

first:
    script:
        - for i in $(seq 1 $ITEMS); do echo "-- ${NAME} --"; done

second:
    dependencies:
        - first
    variables:
        NAME: Jane Doe
        ITEMS: 10
    script:
        - for i in $(seq 1 $ITEMS); do echo "~~ ${NAME} ~~"; done

third:
    dependencies:
        - second
    script:
        - apt-get install -y telnet
        - telnet localhost 32
    can_fail: true
```

### Inspecting the configuration

Listing all targets in the configuration is as easy.

```bash
hammer list
```

Also there's a way of looking at the execution plan:

```bash
hammer plan
```

### Running the configuration

[TODO]

```bash
hammer run
```

## Hammer file reference

[TODO]
