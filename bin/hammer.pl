#!/usr/bin/env perl

use strict;
use warnings;

use Hammer;
use Hammer::Utils qw( dist_file );

use Log::Log4perl;

Log::Log4perl::init( dist_file( 'log4perl.conf' ) );

Hammer->new_with_command()
	->run();
