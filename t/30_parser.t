#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

use_ok( 'Hammer::Parser::YAML' );

done_testing();
