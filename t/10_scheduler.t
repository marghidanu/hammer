#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

use_ok( 'Hammer::Scheduler' );

my $instance = Hammer::Scheduler->new();
isa_ok( $instance, 'Hammer::Scheduler' );
can_ok( $instance, qw( tasks add_task get_execution_plan run ) );

done_testing();
