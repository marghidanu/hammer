#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

use_ok( 'Hammer::Model::Configuration' );
use_ok( 'Hammer::Model::Target' );

done_testing();
