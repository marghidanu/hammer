package Hammer::Parser {
	use Moose;

	use MooseX::AbstractMethod;

	abstract( 'read_file' );

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Parser

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
