package Hammer::Model::Configuration {
	use Moose;

	extends 'Hammer::Model';

	has 'name' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
	);

	has 'description' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
	);

	has 'variables' => (
		is => 'ro',
		isa => 'HashRef[Str]',
		default => sub { {} },
	);

	has 'targets' => (
		is => 'ro',
		isa => 'HashRef[Hammer::Model::Target]',
		default => sub { {} },
		traits => [ qw( Hash ) ],
		handles => {
			'target_names' => 'keys',
			'set_target' => 'set',
			'get_target' => 'get',
			'has_target' => 'exists',
		},
	);

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Model::Configuration

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
