package Hammer::Model::Target {
	use Moose;

	extends 'Hammer::Model';

	has 'description' => (
		is => 'ro',
		isa => 'Maybe[Str]',
		default => undef,
	);

	has 'dependencies' => (
		is => 'ro',
		isa => 'ArrayRef[Str]',
		default => sub { [] },
	);

	has 'variables' => (
		is => 'ro',
		isa => 'HashRef',
		default => sub { {} },
	);

	has 'type' => (
		is => 'ro',
		isa => 'Str',
		default => 'Shell',
	);

	has 'hosts' => (
		is => 'ro',
		isa => 'ArrayRef[Str]',
		default => sub { [] },
	);

	has 'script' => (
		is => 'ro',
		isa => 'Maybe[ArrayRef[Str]]',
		default => sub { [] },
	);

	has 'can_fail' => (
		is => 'ro',
		isa => 'Bool',
		default => 0,
	);

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Model::Target

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
