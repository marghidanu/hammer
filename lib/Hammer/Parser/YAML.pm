package Hammer::Parser::YAML {
	use Moose;

	use YAML::XS qw( LoadFile );

	use Hammer::ModelFactory;

	my @KEYWORDS = qw( name description variables );

	sub read_file {
		my ( $self, $file ) = @_;

		# Loading the data and extracting reserved keywords data
		my $data = LoadFile( $file );
		my $reserved = {
			map { $_ => delete( $data->{ $_ } ) }
				@KEYWORDS
		};

		# Creating the main configuration object
		my $configuration = Hammer::ModelFactory->create( 'Configuration',
			{
				name => $reserved->{name},
				description => $reserved->{description},
				variables => $reserved->{variables} || {},
			}
		);

		# ... and finally dealing with the targets ...
		foreach my $name ( keys( %{ $data || {} } ) ) {
			my $item = $data->{ $name } || {};

			my $target = Hammer::ModelFactory->create( 'Target',
				{
					description => $item->{description},
					dependencies => $item->{dependencies} || [],
					variables => $item->{variables} || {},
					hosts => $item->{hosts} || [],
					script => $item->{script} || [],
					can_fail => $item->{can_fail} || 0,
				}
			);

			$configuration->set_target( $name => $target );
		}

		return $configuration;
	}

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Parser::YAML

=head1 DESCRIPTION

=head1 METHODS

=head2 read_file

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
