package Hammer::Command::Run {
	use MooseX::App::Command;

	extends 'Hammer';

	use Cwd;

	use Hammer::ParserFactory;
	use Hammer::Scheduler;

	option 'directory' => (
		is => 'ro',
		isa => 'Str',
		default => sub { cwd() },
		cmd_aliases => [ qw( d C ) ],
	);

	option 'param' => (
		is => 'ro',
		isa => 'ArrayRef[Str]',
		default => sub { [] },
		cmd_aliases => [ qw( D ) ],
	);

	option 'timeout' => (
		is => 'ro',
		isa => 'Int',
		default => 3600,
	);

	with 'MooseX::Log::Log4perl';

	sub run {
		my $self = shift();

		$self->log()->debug( 'Reading configuration from: ', $self->file() );
		$self->log()->debug( 'Base target: ', $self->target() );
		$self->log()->debug( 'Running in: ', $self->directory() );
		$self->log()->debug( 'Timeout in: ', $self->timeout(), ' seconds' );

		# Parsing additional parameters ...
		my $variables = {};
		foreach my $param ( @{ $self->param() } ) {
			my ( $name, $value ) = split( m/[=]/msx, $param );
			$variables->{ $name } = $value;
		}

		# Creating the parser and loading the configuration ...
		my $configuration = Hammer::ParserFactory->create( $self->parser() )
			->read_file( $self->file() );

		# Creating the scheduler and executing the graph ...
		Hammer::Scheduler->from_configuration( $configuration, $self->target() )
			->run(
				variables => $variables,
				directory => $self->directory(),
				timeout => $self->timeout(),
			);

		return;
	}

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Command::Run

=head1 DESCRIPTION

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
