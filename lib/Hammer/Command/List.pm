package Hammer::Command::List {
	use MooseX::App::Command;

	extends 'Hammer';

	use Hammer::ParserFactory;

	use Text::Table;

	with 'MooseX::Log::Log4perl';

	sub run {
		my $self = shift();

		$self->log()->info( 'Reading configuration from: ', $self->file() );
		my $configuration = Hammer::ParserFactory->create( $self->parser() )
			->read_file( $self->file() );

		my $table = Text::Table->new();
		$table->load(
			map { [ $_, $configuration->get_target( $_ )->description() ] }
				sort( $configuration->target_names() )
		);

		printf( "%s\n", $table );

		return;
	}

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Command::List

=head1 DESCRIPTION

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
