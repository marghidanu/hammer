package Hammer::Command::Plan {
	use MooseX::App::Command;

	extends 'Hammer';

	use Hammer::ParserFactory;
	use Hammer::Scheduler;

	with 'MooseX::Log::Log4perl';

	sub run {
		my $self = shift();

		$self->log()->info( 'Reading configuration from: ', $self->file() );
		my $configuration = Hammer::ParserFactory->create( $self->parser() )
			->read_file( $self->file() );

		my @topology = Hammer::Scheduler->from_configuration(
			$configuration,
			$self->target(),
		)->get_execution_plan();

		foreach my $name ( @topology ) {
			my $target = $configuration->get_target( $name );

			printf( "\"%s\"\n%s\n\n",
				$name,
				$target->description() || 'No description',
			);
		}

		return;
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

Hammer::Command::Plan

=head1 DESCRIPTION

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
