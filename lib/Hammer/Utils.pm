package Hammer::Utils {
	use Moose;
	use Moose::Exporter;

	use File::ShareDir;

	our $DIST = 'Hammer';

	Moose::Exporter->setup_import_methods(
		as_is => [ qw( dist_dir dist_file ) ]
	);

	sub dist_dir {
		return File::ShareDir::dist_dir( $DIST );
	}

	sub dist_file {
		my @args = @_;

		return File::ShareDir::dist_file( $DIST, @args );
	}

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Utils

=head1 DESCRIPTION

=head1 METHODS

=head2 dist_dir

=head2 dist_file

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
