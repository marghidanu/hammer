package Hammer::Scheduler {
	use Moose;

	use Graph::Directed;

	use Time::Out qw( timeout );

	use Carp qw( croak );

	use Hammer::Scheduler::TaskFactory;

	use constant {
		DEFAULT_TIMEOUT => 3600,
	};

	has '_graph' => (
		is => 'ro',
		isa => 'Graph::Directed',
		default => sub {
			return Graph::Directed->new()
		},
	);

	has 'tasks' => (
		is => 'ro',
		isa => 'HashRef[Hammer::Scheduler::Task]',
		default => sub { {} },
		traits => [ qw( Hash ) ],
		handles => {
			'_set_task' => 'set',
			'_get_task' => 'get',
		},
	);

	with 'MooseX::Log::Log4perl';

	sub add_task {
		my ( $self, $name, $task, @deps ) = @_;

		$self->_set_task( $name, $task );

		$self->_graph()->add_vertex( $name );
		$self->_graph()->add_edge( $_, $name )
			foreach( @deps );

		return $self;
	}

	sub get_execution_plan {
		my $self = shift();

		return $self->_graph()->topological_sort();
	}

	sub run {
		my ( $self, %params ) = @_;

		timeout $params{timeout} || DEFAULT_TIMEOUT => sub {
			my @topology = $self->get_execution_plan();
			foreach my $name ( @topology ) {
				my $task = $self->_get_task( $name );
				my $code = $task->run( %params );

				if( $code ) {
					if( !$task->can_fail() ) {
						$self->log()->error( "Task '${name}' exited with code ${code}." );
						last();
					}

					$self->log()->warn( "Task '${name}' exited with code ${code} but can continue." );
				}
			}
		};

		croak( sprintf( 'Operation timed out after %d seconds', $params{timeout} ) )
			if( my $error = $@ );

		return;
	}

	sub from_configuration {
		my ( $class, $configuration, $root ) = @_;

		my $scheduler = $class->new();
		_walk_configuration( $scheduler, $configuration, $root );

		return $scheduler;
	}

	sub _walk_configuration {
		my ( $scheduler, $configuration, $name ) = @_;

		# Validate if target exists in the list ...
		croak( "Task '${name}' was not found!" )
			unless( $configuration->has_target( $name ) );

		# ... an retrive it
		my $target = $configuration->get_target( $name );

		# Also assemble variables list and make sure they are in the correct order
		my $variables = {
			%{ $configuration->variables() },
			%{ $target->variables() },
		};

		# ... now we create the task object with all the required components
		my $task = Hammer::Scheduler::TaskFactory->create( $target->type(),
			{
				name => $name,
				script => $target->script(),
				variables => $variables,
				hosts => $target->hosts(),
				can_fail => $target->can_fail(),
			}
		);

		# Going down the dependencies list ...
		_walk_configuration( $scheduler, $configuration, $_ )
			foreach ( @{ $target->dependencies() } );

		# and adding the task to the scheduler.
		$scheduler->add_task( $name, $task, @{ $target->dependencies() } );

		return;
	}

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Scheduler

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 DEFAULT_TIMEOUT

=head1 METHODS

=head2 from_configuration

=head2 add_task

=head2 get_execution_plan

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
