package Hammer::ModelFactory {
	use MooseX::AbstractFactory;

	implementation_class_via sub {
		return sprintf( 'Hammer::Model::%s', shift() );
	};
}

1;

__END__

=head1 NAME

Hammer::ModelFactory

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
