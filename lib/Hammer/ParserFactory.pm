package Hammer::ParserFactory {
	use MooseX::AbstractFactory;

	implementation_class_via sub {
		return sprintf( 'Hammer::Parser::%s', shift() );
	};
}

1;

__END__

=head1 NAME

Hammer::ParserFactory

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
