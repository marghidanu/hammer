package Hammer::Scheduler::Task {
	use Moose;

	use MooseX::AbstractMethod;

	has 'name' => (
		is => 'ro',
		isa => 'Str',
		required => 1,
	);

	has 'hosts' => (
		is => 'ro',
		isa => 'ArrayRef[Str]',
		default => sub { [] },
		traits => [ qw( Array ) ],
		handles => {
			'has_hosts' => 'count',
		},
	);

	has 'script' => (
		is => 'ro',
		isa => 'ArrayRef[Str]',
		default => sub { [] },
	);

	has 'variables' => (
		is => 'ro',
		isa => 'HashRef',
		default => sub { {} },
	);

	has 'can_fail' => (
		is => 'ro',
		isa => 'Bool',
		default => 0,
	);

	abstract( 'run' );

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Scheduler::Task

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
