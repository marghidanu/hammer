package Hammer::Scheduler::Task::Shell {
	use Moose;

	extends 'Hammer::Scheduler::Task';

	use Shell::Cmd;

	with 'MooseX::Log::Log4perl';

	sub run {
		my ( $self, %params ) = @_;

		# NOTE: The order is important in this case, command line variables
		# must override the the ones defined in configuration/target.
		my %variables = (
			%{ $self->variables() },
			%{ $params{variables} || {} },
		);

		my $shell = Shell::Cmd->new();
		$shell->options(
			echo => 'failed',
		);

		# Setting the working directory if defined ...
		$shell->dire( $params{directory} )
			if( exists( $params{directory} ) );

		$shell->env( %variables );
		$shell->cmd( @{ $self->script() } );

		# Determining where to run, local shell or SSH?
		my $message = sprintf( 'Task "%s" on: %s',
			$self->name(),
			( $self->has_hosts() ? join( ', ', @{ $self->hosts() } ) : '(local)' )
		);

		$self->log()->info( $message );
		my ( $error ) = $self->has_hosts() ?
			$shell->ssh( @{ $self->hosts() } ):
			$shell->run();

		return $error;
	}

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer::Scheduler::Task::Shell

=head1 DESCRIPTION

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
