package Hammer::Scheduler::TaskFactory {
	use MooseX::AbstractFactory;

	implementation_class_via sub {
		return sprintf( 'Hammer::Scheduler::Task::%s', shift() );
	};
}

1;

__END__

=head1 NAME

Hammer::Scheduler::TaskFactory

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
