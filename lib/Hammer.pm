package Hammer {
	use MooseX::App qw( Version );

	our $VERSION = '0.0.0';

	option 'file' => (
		is => 'ro',
		isa => 'Str',
		default => 'hammer.yaml',
		cmd_aliases => [ qw( f ) ],
	);

	option 'parser' => (
		is => 'ro',
		isa => 'Str',
		default => 'YAML',
		cmd_aliases => [ qw( p ) ],
	);

	parameter 'target' => (
		is => 'ro',
		isa => 'Str',
		default => 'default',
	);

	app_permute( 1 );
	app_namespace( 'Hammer::Command' );

	__PACKAGE__->meta()->make_immutable();

	no Moose;
}

1;

__END__

=head1 NAME

Hammer

=head1 DESCRIPTION

=head1 METHODS

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
