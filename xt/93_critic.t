#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Perl::Critic (
	-severity => 1,
	-exclude => [
		'strict',
		'warnings',
		'constant',
		'RequireTidyCode',
		'RequireVersionVar',
		'ProhibitParensWithBuiltins',
	],
);

all_critic_ok();
